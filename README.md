# Transbox
This is a tool I made for myself, but I thought others would find this useful tool, so here it is. It's aimed towards Linux users like myself, who have an iPod with Rockbox, but hate the weirdness iPods come with handling on Linux.
I also further updated this tool to work nicely with converting music to work on the PSP, and as a bonus, the DSi.

## What does it do?
- Transcodes the files to a given format (i.e. from and to FLAC/MP3/OGG/WAV/M4A) using pydub. Mainly useful for converting FLACs to lossy compression.
- It uses a settings file to allow a lot of customization, such as:
    - "uni_decode" can remove all non-ASCII text, since iPod storage is weird and doesn't like that. This also transliterates the file & folder names (i.e. "5 YEARS メガミックス" turns into "5 YEARS megamitsukusu").
    - "album_size" resizes album art to the appopriate size, and saves them as non-progressive JPEGs, so they're compatible with Rockbox, and doesn't lag systems like the PSP. This does crop album art that doesn't have a 1:1 aspect ratio, but it does crop it to the center. This also assumes you have a file named "cover.png" or "cover.jp(e)g" in the folder, it won't work otherwise.
    - "force_transcode" will transcode files that are already the desired format. Useful if you have MP3s that just don't play nicely with your device for any given reason, and it will also re-inject the album art to make the files use smaller sizes of album art.
- This will strip non-FAT32 friendly characters, to prevent any file-copying conflicts. (anything invalid will be replaced with a "_")
- If you supply a file path as an argument, it'll use that as the settings file. Useful for managing different devices.
- It also does not grab any non-music file (outside of cover art), and doesn't go deeper than 1 sub-directory.


## How do I use it?
Make sure you have **Python** installed, and **PIP** installed. You'll also need **ffmpeg** installed for **pydub** to function. From the root folder, run `pip -r requirements.txt` (or `python3 -m pip -r requirements.txt` , or `python -m pip -r requirements.txt`) to install all needed modules. You can open "*settings.yaml*" to configure what format to transcode to, to disable unidecode (good for HFS iPods, I think anyways), and automatically set the paths of your Music folder, and your device. There's also presets for certain devices like the PSP and DSi to give you the right settings for them. Then, run it from a terminal (`python3 main.py`, or `python main.py`) and you're good to go! You can also tell it to look at a different settings file like this: `python3 main.py some-other-file.yaml`
