from yaml import safe_load
import os
import shutil
import pathlib
from PIL import Image
from unidecode import unidecode
import sys
import time

import pydub

#TODO: improve detection of already existing files when using force_sort_by_track_number
#TODO: also make force sort staple the track number to the front of the file as an extra option, to really make sure things are sorted right

settings_file_path = "settings.yaml"
if len(sys.argv) == 2:
    settings_file_path = sys.argv[1]

try:
    settings_file = open(settings_file_path, 'r')
except:
    print("Could not load settings file '" + settings_file_path + "'! Trying settings.yaml instead...")
    time.sleep(1)
    try:
        settings_file_path = "settings.yaml"
        settings_file = open(settings_file_path, 'r')
    except:
        print("Could not load settings file '" + settings_file_path + "'! Aborting!")
        exit()

settings = safe_load(settings_file)
settings_file.close()

musicfolder = settings["file_paths"]["music_path"]
devicepath = settings["file_paths"]["device_path"]

givepathnotice = False

transcodeto = settings["main_options"]["format_to_transcode_to"]
doidecode = settings["main_options"]["uni_decode"]
forcetranscode = settings["main_options"]["force_transcode"]
maximagesize = settings["main_options"]["album_size"]
copyalbumart = settings["main_options"]["copy_album_art"]
forcesort = settings["main_options"]["force_sort_by_track_number"]

preset = settings["preset"]["option"]

if preset != "none":
    if preset == "dsi":
        transcodeto = ".m4a"
        copyalbumart = False
        print("DSi preset selected, transcoding to .m4a, and omitting album art.")
    elif preset == "psp":
        transcodeto = ".mp3"
        if copyalbumart: #only reason to force a transcode is for the album art, which is done to resize album art
            forcetranscode = True
            if maximagesize > 272:
                maximagesize: 272 #maximum size of the PSP screen, i don't think you can zoom in to the album art, but if you can, this is good enough to prevent lag
        print("PSP preset selected, transcoding to .mp3, resizing album art if above 272, and forcing transcode if album art is enabled.")
    elif preset == "rockbox":
        doidecode = True #only thing we need to do really lol
        print("Rockbox preset selected, decoding UNI.")
    #elif preset == "3ds":
        #transcodeto = ".mp3"
        #copyalbumart = False
        #print("3DS preset selected, transcoding to .mp3, and omitting album art.")
    else:
        print("Invalid preset option given! Aborting!")
        exit()



fileExt = [
    ".ogg",
    ".mp3",
    ".wav",
    ".flac",
    ".wma",
    ".mp4"
]

def decoder(string):
    if doidecode:
        string = unidecode(string).replace("/", "_")
    string = string.replace("*", "_")
    string = string.replace("<", "_")
    string = string.replace(">", "_")
    string = string.replace(":", "_")
    string = string.replace('"', "_")
    string = string.replace("\\", "_")
    string = string.replace("|", "_")
    string = string.replace("?", "_")
    string = string.rstrip() #because decode likes to be funny and leave trailing spaces
    string = string.lstrip() #just a preventitive measure now
    return string

#TODO: add a quick test for pydub here, and if it fails, it might mean ffmpeg isn't installed. tell the user to install ffmpeg!

def checkpath(var, settingname):
    if var: #os.path.exists errors out if it's blank, for some reason
        if not os.path.exists(var):
            print("WARNING! '" + settingname + "' is not set correctly in '" + settings_file_path + "'! Requiring user input now...")
            return False
        else:
            return True

if not checkpath(musicfolder, "music_path"): musicfolder = ""
if not checkpath(devicepath, "device_path"): devicepath = ""

#if empty, prompt the user for the paths
if not musicfolder:
    while True:
        musicfolder = input("Please put in the full path for the music folder: \n")
        if os.path.exists(musicfolder):
            break
        else:
            print("Invalid path, please try again.")
    givepathnotice = True

if not devicepath:
    while True:
        devicepath = input("Now, put in the music path for your Rockbox device's Music folder: \n")
        if os.path.exists(devicepath):
            break
        else:
            print("Invalid path, please try again.")
    givepathnotice = True

#remove the "/" if it's at the end, so we don't have a bunch of / at the end
musicfolder = musicfolder.rstrip('/')
devicepath = devicepath.rstrip('/')

if givepathnotice:
    print("Note for next time: You can have the paths automatically set if you edit '" + settings_file_path + "'!")
else:
    print("Loaded paths from '" + settings_file_path + "' successfully")

print("Original music folder: " + musicfolder)
print("Folder to transcode to: " + devicepath)
print("File type to transcode to: " + transcodeto)

musicitems = os.listdir(musicfolder)
musicnest = []
musicstandalone = []

if forcesort:
    print("Force sort is enabled! This might take awhile!")

for item in musicitems:
    if not item.startswith("."): #don't do dotfiles
        combined = musicfolder + "/" + item
        if os.path.isdir(combined):
            thedict = {"dir": item, "fulldir":combined,"files":[],"cover":""}
            nest = os.listdir(combined)
            files = []
            thecover = ""
            for nestitem in nest:
                if os.path.splitext(nestitem)[0] == "cover": #get cover art
                    thedict["cover"] = nestitem
                    thecover = nestitem
                else:
                    files.append(nestitem)

            files = sorted(files) #sorts the files

            if forcesort:
                print("Current directory: " + item)
                shouldwe = False #first, check to make sure we didn't already write the files. this process takes awhile, so, skip it if we can!
                #check first to see if files exist, if they don't, proceed
                for file in files:
                    splitextension = os.path.splitext(file)
                    filename = splitextension[0] + transcodeto
                    filename = decoder(filename)
                    dir2write = decoder(item.rstrip('/')) + '/'
                    file2make = devicepath + "/" + dir2write + filename #full path of the file we will create

                    #print("gloop: " + file2make)
                    if (not os.path.exists(file2make)) and not shouldwe:
                        #print("we should")
                        shouldwe = True
                #filewithtrack = {"files": [], "tracks": []}
                if shouldwe:
                    tracks = []
                    try:
                        for file in files:
                            track = int(pydub.utils.mediainfo(combined+"/"+file).get('TAG', {}).get('track'))
                            #filewithtrack["files"].append(files)
                            #print(file)
                            #filewithtrack["tracks"].append(track)
                            tracks.append(track)
                            #print(track)
                        #now we sort
                        filetracks = zip(tracks, files)
                        filetracks = sorted(filetracks)
                        files = [x for y, x in filetracks]
                        #print(files)
                    except:
                        print("Can't get track numbers! These tracks may not be properly numbered, or are on multiple discs!")
                #else:
                    #print("skipping, we don't need to")

            if thecover and copyalbumart: #don't put this in if the user don't want it
                files.insert(0, thecover) #now, make the cover the first item, so we get to that first. important for mp3 files!
            thedict["files"] = files
            if len(files) > 0: #skips empty folders
                musicnest.append(thedict)
        else:
            musicstandalone.append(item) #item is a standalone file at the top, i.e. a single track

#print(musicnest)

if musicstandalone:
    musicnest.append({"dir": "!¡!ROOTFOLDERYAY!¡!", "fulldir":musicfolder, "files":musicstandalone, "cover":""}) #god help you if someone names their album "!¡!ROOTFOLDERYAY!¡!"

for nest in musicnest:
    #print(nest)
    thedir = nest["dir"] + "/"
    if nest["dir"] == "!¡!ROOTFOLDERYAY!¡!":
        thedir = ""

    dir2write = thedir

    if transcodeto:
        dir2write = decoder(dir2write.rstrip('/')) + '/' #somehow it's just easier to strip the character at this point, then put it back in. unless i'm wrong
    
    if thedir != "": #ignore, it's root
        if not os.path.exists(devicepath + "/" + dir2write):
            os.mkdir(devicepath + "/" + dir2write)
            print("Making directory: " + devicepath + "/" + dir2write)

    for file in nest["files"]:
        splitextension = os.path.splitext(file) #split file into file and extension
        ogfile = musicfolder + "/" + thedir + file
        filename = splitextension[0] + transcodeto
        if transcodeto:
            filename = decoder(filename)
        file2make = devicepath + "/" + dir2write + filename #full path of the file we will create
        print(file + " --> " + dir2write + filename)

        if file == nest["cover"] and not os.path.exists(devicepath + "/" + dir2write + splitextension[0] + ".jpg"): #convert album cover art to work on rockbox
            im = Image.open(ogfile, "r")
            b4thumb = im.copy()
            thumbwidth = maximagesize
            thumbheight = maximagesize

            if (im.width < thumbwidth) or (im.height < thumbheight):
                #print("original art is too small, resizing")
                im = im.resize((thumbwidth, thumbheight))

            im.thumbnail((thumbwidth, thumbheight))

            def thumbnailresizer():
                #print("image is too small, resizing thumbnail portion to: " + str(thumbwidth) + ", " + str(thumbheight))
                im = b4thumb.copy() #restore copy of original image
                im.thumbnail((thumbwidth, thumbheight))
                #print("thumbw: " + str(im.width))
                #print("thumbh: " + str(im.height))
                return im

            resizeiterations = 0
            maxresizeops = 250 #prevents this from being stuck in an endless loop
            if im.width/im.height != 1: #prevents 1:1 images being messed with... hopefully
                while not (((im.width > maximagesize) and (im.height > maximagesize))
                    or ((b4thumb.width < maximagesize) or (b4thumb.width < maximagesize))
                    or resizeiterations > maxresizeops):
                    resizeiterations += 1
                    if resizeiterations == 1:
                        print("Resizing cover...")
                    thumbheight += 5 #do the adding here, doesn't work in function for some reason
                    thumbwidth += 5
                    im = thumbnailresizer()
            else:
                thumbheight = maximagesize
                thumbwidth = maximagesize

            if resizeiterations > maxresizeops:
                print("resize iterations went over " + str(maxresizeops) + "! operation was interrupted")
            
            b4thumb.close()

            #TODO: unfortunate byproduct is that thumbnails that are already square get resized by a tiny bit anyways. maybe fix that?

            width, height = im.size
            left = (width - maximagesize)/2
            top = (height - maximagesize)/2
            right = (width + maximagesize)/2
            bottom = (height + maximagesize)/2

            im = im.crop((left, top, right, bottom)) #crop it to the center as a Square
            im = im.convert('RGB') #convert to RGB since jpegs don't have alpha
            #TODO: before converting, consider drawing a white square behind it... somehow, and only if it's needed
            im.save(devicepath + "/" + dir2write + splitextension[0] + ".jpg", "JPEG", quality=85, progressive=False) #image HAS to not be progressive, forcing it just in case
            im.close()
        
        if not os.path.exists(file2make) or os.stat(file2make).st_size == 0: #latter is to check if the file size is empty; if the script was interrupted it generates an empty file
            if (splitextension[1] == transcodeto) and not forcetranscode: #check if file is already our goal file, if so, just copy it instead
                print("The file is already a " + transcodeto + ", just copying.")
                shutil.copyfile(ogfile, file2make)
            else: #fun transcoding part
                #print(ogfile, os.path.splitext(ogfile)[1])
                if os.path.splitext(ogfile)[1] in fileExt:
                    print("Loading audio...")
                    music = pydub.AudioSegment.from_file(ogfile)
                    print("Converting...")
                    if transcodeto == ".mp3" and copyalbumart: #apply cover art to file itself, mainly for PSP
                        #mp3album = musicfolder + "/" + thedir + "cover" + ".jpg"
                        mp3album = devicepath + "/" + dir2write + "cover" + ".jpg" #this uses the newly generated cover file, which will be the first file made.
                        if not os.path.exists(mp3album): #just in case we don't have album art, for some reason. would rather not error out the script and waste someone's (my) time
                            mp3album = None
                    else:
                        mp3album = None
                    theformat = transcodeto[1:]
                    if transcodeto == ".m4a":
                        theformat = "mp4"
                    music.export(file2make, format=theformat, tags=pydub.utils.mediainfo(str(ogfile)).get('TAG', {}), cover=mp3album)
        #else:
            #print("File already exists!")
        
